const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipie');

const app = express()

//middleware para entender json
app.use(express.json())


//mongodb connection
//mongoose.connect('mongodb://admin:admin@localhost:27017/recetario?authSource=admin')
mongoose.connect('mongodb://admin:admin@mongo:27017/recetario?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error));

app.get('/', (req, res) => {
    res.send('Api del recetario')
});

app.post('/new_user', (req, res) => {
    const user = userSchema(req.body);
    user.save()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error))
});

app.get('/users', (req, res) => {
    const user = userSchema(req.body);
    userSchema.find()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error))
});

app.post('/new_recipe', (req, res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error))
});

app.post('/rate', (req, res) => {
    const {recipeId, userId, rating} = req.body;    
    recipeSchema.updateOne(
        {_id: recipeId},
        [ // pipeline para ejecutar aggregations en MongoDb
            {$set: {ratings: { $concatArrays : [ 
                { $ifNull: [ '$ratings', [] ] },  
                [{ user: userId, rating: rating} ] 
            ] } } },// primera etapa del pipeline
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}//seteando el average rating
        ]
    )
    .then( (data) => res.json(data))
    .catch( (error) => {
        console.log(error);
        res.send(error);    
    });
});

app.get('/recipies', (req, res) => {
    const {userId, recipeId}= req.body;
    if (recipeId){
        recipeSchema.find( { _id: recipeId })
        .then( (data) => res.json(data))
        .catch( (error) => res.send(error))
    }else{
        if(userId){
            recipeSchema.find( { userId: userId })
            .then( (data) => res.json(data))
            .catch( (error) => res.send(error))
        }else{
            recipeSchema.find()
            .then( (data) => {
                res.json(data);
            })
            .catch( (error) => {
                console.log(error);
                res.send(error);
            });
        }
    }
});


app.get('/recipesbyingredient', (req, res) => {
    // res.send('enviamos las recetas')
    // TODO: buscar recetas por ingredientes
    const {ingredients} = req.body;
    const ingredientsArray = ingredients.flatMap(obj => Object.values(obj));
    const pipeline = [
        { $unwind: "$ingredients" }, 
        { $match: { "ingredients.name": {$in: ingredientsArray } } }, 
        { $group: {_id: "$_id"  } }
    ];
    console.log(ingredientsArray.length)
    recipeSchema.aggregate(pipeline)
    .then( (data) => {
        const idArray = data.flatMap(obj => Object.values(obj));
        console.log(idArray);
        recipeSchema.find({ _id: { $in : idArray}})
        .then( (data) => res.json(data))
        .catch( (error) => res.send(error));
    })
    .catch( (error) => res.send(error));
    
});

app.listen(3000, () => {console.log("escuchando en el puerto 3000...")});